PL = {
    "language": "polski",
    "bot_start":
    """
Hej, {user_name}!
Jestem {bot_name}
Mogę cię powiadamiać o nowych futrach z okolicy.

Żeby zacząć otwórz /ustawienia i zmień lokalizację,
lub wyślij swoją lokalizację z menu załączników

wszystkie komendy - /pomoc 
więcej informacji - /tos

<i>Baza danych furmap.net jest sprawdzana raz na dzień(więc odświerzanie profili więcej niż raz na dzień nic nie da),
jeśli masz jakieś uwagi lub znalazłeś błąd/bug pisz do @radtek
lub napisz w <a href=\"https://codeberg.org/neoGAN/furmap_notifier_v2\">repo bota</a></i>
Dołącz do @RTTlab (pl) lub @ocstuff (en) dla przyszłych projektów i aktualizacji bota
""",


    "bot_help":
    """
<b>Wszystkie komendy:</b>
 {commands}

 informacje i inne ustawienia są dostępne w /ustawienia
""",


    "tos":
    """
ToS/polityka prywatności/coś takiego:
1. ten bot nie jest w żaden sposób związany z furmap.net 
2. Twoja lokaliazja jest lekko randomizowana przed zapisaniem 
3. Możesz rozpocząć usunięcie danych komendą /usunDane
4. Ten bot zapisuje tylko informacje które dajesz i są potrzebne do do działania:
  - twoje ID telegram
  - lokalizacja
  - obszar szukania
  - język
5. Ten bot podaje informacje które dostaje od furmap.net więc lokalizacje kont są niedokładne
6. kod źródłowy bota znajduje się <a href=\"https://codeberg.org/neoGAN/furmap_notifier_v2\">tutaj</a>
""",


    "setup_location": "Najpierw ustaw swoją lokalizację",
    "True": "Tak",
    "False": "Nie",
    "botTrue": "włączony",
    "botFalse": "wyłączony",
    "refresh": "odśwież",
    "visit_account": "odwiedź konto",
    "show_location": "wyślij lokalizację",
    "open_settings": "otwórz ustawienia",
    "no_changes": "nic się nie zmieniło",
    "location_change_ok": "Lokalizacja zmieniona pomyślnie",
    "location_not_available": "sprawdź komendą /lokalizacja",
    "failure_input_parse": "Upewnij się że parametry są poprawne",
    "failure_input_place": "Upewnij się że parametry są poprawne.\nPrzykład: <code>/place warszawa</code>",
    "failure_input_setarea": "Upewnij się że parametry są poprawne.\nPrzykład: <code>/setAarea 30</code>",
    "failure_input_setarea_number": "Możesz wybrać max 100km",
    "setarea_ok": "Gotowe, obszar szukania został zmieniony na {area}km",
    "place_error_empty": "Wymagane conajmniej 4 znaki",
    "unknown_cmd_error": "Coś poszło nie tak, spróbuj jeszcze raz",
    "limit_reached": "Poczekaj do pełnej godziny zanim użyjesz tą komendę ponownie",
    "location_limit_reached": "Poczekaj do pełnej godziny zanim użyjesz tą komendę ponownie lub wyślij swoja lokalizację z menu załączników",
    "geocache_request_failed": "Nie otrzymałem prawidłowej odpowiedzi od serwera, spróbuj wpisać cos innego lub poczekaj chwilę",
    "location_save_error": "Nie udało się zapisać twojej lokalizacji, spróbuj ponownie później",
    "confirm_deldata": "potwierdź usunięcie konta poprzez wciśnięcie przycisku poniżej",
    "deldata_done": "Twoje konto zostanie usunięte w ciągu 24 godzin chyba że użyjesz bota w ciągu tego okresu.",
    "showing_less_users": "Pokazuję 3 z {len} osób.\nŻeby pokazać więcej poczekaj godzinę lub wyślij /odswiez \nMożesz jeszcze /odznacz -yćte konta jako przeczytane",
    "no_new_accs": "Brak nowych kont",
    "new_acc_found": "🔔 Nowe konto/a w okolicy znalezione!",
    "km_from": " od ciebie",
    "all_accs_nearby": "Wszystkie konta w twojej okolicy:",
    "seen": "Ok, odznaczono {seen} kont",
    "acc_404": "Martwe konto",
    "no_acc_yet": "Nie masz konta lub jest już w trakcie usuwania",
    "deprecated_cmd": "Ta komenda już nie istnieje, teraz wszytsko jest zarządzane w /ustawienia (ch)",
    "processing": "Przetwarzanie, czekaj..",
    "broabcast_unsub": "Nie subskrybujesz już do aktualizacji o bocie.\nŻeby włączyć je z powrotem wyślij /subscribe",
    "broabcast_sub": "Będziesz otrzymywał teraz powiadomienia o nowych aktualizacjach i nie tylko.\nŻeby je wyłączyć możesz wysłać /unsubscribe",
    "place_data": "Ok: <code>{formatted}</code>\n\nTip:\n<i>Jeśli nie jesteś pewien czy to jest to miejsce wyślij /lokalizacja i sprawdź na mapie</i>",
    "set_lang_successful": "Język zmieniony pomyslnie",
    "set_location_info": "Wyślij teraz swoją lokalizację przez menu załączników albo wpisz swoje miasto np. \"<code>wejherowo, polska</code>\"",
    "set_area_info": "Wyślij teraz jaki obszar mam szukać w kilometrach",
    "make_sure_location_correct": "zapytanie ::<code>{location}</code>::\n\n zwróciło wiele wyników, wybierz wynik który miałeś na myśli",
    "action_reset": "Anulowano.",
    "back": "Wstecz",
    "data_expired": "Dane z tej wiadomości wygasły. Spróbuj ponownie.",
    "lang_error": "Ups, nie powinieneś tego widzieć. Nie można znaleźć odpowiedzi dla twojego języka. Proszę zgłoś to.",

    "furmap_user_card_text":
    """
<a href=\"{profileURL}\">{name}</a>, ~{distance}{distUnit} od ciebie
{socialMedia}

----@{bot_username}----
""",

    "settings_panel_info": """
ustawienia {bot_name}: 
Bot {is_on_verb}

Aktualna lokalizacja: {location}
Na mapie: /lokalizacja
Obszar szukania: {area}{distUnit_verb}
Przekierowywanie do prywatnych stron: {privateLinks_verb}

<b>{warning}</b>
    """,
    "setup_start_btn": "Skonfiguruj bota",
    "bot_toggle_btn": "wyłącz/włącz bota",
    "lang_change_btn": "Zmień język",
    "location_set_btn": "Zmień lokalizację",
    "area_set_btn": "Zmień obszar szukania",
    "dist_unit_btn": "Zmień jednostkę odległości",
    "priv_link_btn": "Przekierowuj do prywatnych stron",

    "CMDS": {
        "settings": ["ustawienia", "otwórz ustawienia"],
        "help": ["pomoc", "pokaż wszystkie komendy"],
        "tos": ["tos", "info o bocie"],
        "setarea": ["zmienObszar", "zmień obszar szukania"],
        "setlocation": ["zmienPolozenie", "zmień gdzie mam szukać"],
        "start": ["start", "pokaż informację powitalną"],
        "setlang": ["zmienJezyk", "zmień język bota"],
        "refresh": ["odswiez", "sprawdź ręcznie czy ktoś się pojawił"],
        "deldata": ["usunDane", "usuń swoje dane z bota"],
        "seen": ["odznacz", "oznacz nowe konta jako przeczytane"],
        # "faq":["faq", "więcej informacji o bocie"],
        "getlocation": ["lokalizacja", "sprawdź swoją lokalizację"]
    }
}

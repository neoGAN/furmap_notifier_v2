EN = {
    "language": "english",
    "bot_start":
    """
Hey, {user_name}!
I'm {bot_name}
I can notify you about new furs in your area.

To start open /settings, set your location and change other settings to your taste

<i>furmap.net database is checked once every day(so refreshing profiles multiple times a day just lowers your credits),
if you have any trouble, feedback or found a bug write to @radtek
or start an issue on <a href=\"https://codeberg.org/neoGAN/furmap_notifier_v2\">bot's repo</a></i>

Join @ocstuff (en) or @RTTlab (pl) for future updates and projects!
""",


    "bot_help":
    """
<b>All commands:</b>
 {commands}

 other stuff can be set from the /settings menu
""",


    "tos":
    """
ToS/privacy policy:
1. This bot is not associated with furmap.net in any way
2. Your location, randomized a little before saved
3. You can delete your information at any time using command <code>/deldata</code>
4. This bot collects only what you give it and what is required to work:
  - your telegram ID
  - location
  - area you want to search
  - language
5. This bot provides the same location furmap.net does, so keep in mind it's not accurate
6. bot's repo is located <a href=\"https://codeberg.org/neoGAN/furmap_notifier_v2\">here</a>
""",

    "setup_location": "Setup your location first",
    "True": "Yes",
    "False": "No",
    "botTrue": "on",
    "botFalse": "off",
    "refresh": "refresh",
    "visit_account": "visit account",
    "show_location": "show location",
    "open_settings": "open settings",
    "no_changes": "nothing changed",
    "location_change_ok": "Location set succesfully",
    "location_not_available": "check with /mylocation",
    "failure_input_parse": "Make sure your parameters are correct",
    "failure_input_place": "Make sure your parameters are correct.\nexample: <code>/place england london</code>",
    "failure_input_setarea": "Make sure your parameters are correct.\nexample: <code>/setarea 30</code>",
    "failure_input_setarea_number": "You can set area to 1-{AREA_LIMIT}",
    "setarea_ok": "Done, your search area was set to {area}km",
    "place_error_empty": "you have to input min. 4 characters",
    "unknown_cmd_error": "Something went wrong, try typing that again",
    "limit_reached": "You reached daily usage limit of this command.",
    "location_limit_reached": "You reached daily usage limit of this command.\nIt's still possible to set location via attachemts menu though.",
    "geocache_request_failed": "Couldn't get valid reply from server, try writing something else or try again later.",
    "location_save_error": "Couldn't get your location, try again later",
    "confirm_deldata": "press the \"yes\" button below to confirm you want to delete your account",
    "deldata_progress": "Deleting in progress...",
    "deldata_done": "Your data will be deleted within the next 24 hours unless you interact with bot.",
    "showing_less_users": "Showing 3 out of {len} people. Send /refresh to show more, or mark them as /seen",
    "no_new_accs": "No new frens found >:",
    "new_acc_found": "🔔 New account(s) found!",
    "km_from": " away",
    "all_accs_nearby": "All nearby accounts:",
    "seen": "Ok, seen {seen} accounts",
    "acc_404": "Account not found",
    "no_acc_yet": "You don't have an account or it's pending deletion already",
    "deprecated_cmd": "This command no longer exists.\nNow everything is managed in /settings",
    "processing": "Processing, please wait...",
    "broabcast_unsub": "You're no longer subscribed to updates.\nTo subscribe again send /subscribe",
    "broabcast_sub": "You'll now recive updates about me. To unsubscribe send /unsubscribe",
    "place_data": "Ok: <code>{formatted}</code>\n\nTip:\n<i>It you're not sure that's the right place /myLocation provides your currently set location</i>",
    "set_lang_successful": "Language set successfully!",
    "set_location_info": "Now send your location via the attachments menu or text your location, for example: \"<code>London, England</code>\"",
    "set_area_info": "Ok, now send the area I'll search in kilometers\nor just tell me it's in miles and I'll convert it",
    "make_sure_location_correct": "::<code>{location}</code>::\n\nreturned multiple results, please select the one you meant",
    "action_reset": "Action cancelled.",
    "back": "Back",
    "cancel": "cancel",
    "data_expired": "Sorry, data from this message expired. Try again.",
    "lang_error": "Oops, you shouldnt've seen this. I couldn't find proper response for your language, please report this",

    "furmap_user_card_text":
    """
<a href=\"{profileURL}\">{name}</a>, ~{distance}{distUnit} away
{socialMedia}

----@{bot_username}----
""",

    "settings_panel_info": """
{bot_name}'s settings: 
Bot is {is_on_verb}

Current location: {location}
On map: /myLocation
Search area: {area}{distUnit_verb}
serve links to privacy respecting alternatives: {privateLinks_verb}

<b>{warning}</b>
    """,
    "setup_start_btn": "Configure bot",
    "bot_toggle_btn": "turn bot on/off",
    "lang_change_btn": "Change language",
    "location_set_btn": "Change location",
    "area_set_btn": "Set search area",
    "dist_unit_btn": "Toggle units",
    "priv_link_btn": "Redirect to private alternatives",

    "CMDS": {
        "settings": ["settings", "open settings"],
        "help": ["help", "see all commands"],
        "tos": ["tos", "terms of service and other"],
        "setarea": ["setArea", "set search radius"],
        "setlocation": ["setLocation", "set where to search"],
        "start": ["start", "show start message"],
        "setlang": ["lang", "set your language"],
        "refresh": ["refresh", "check manually for new accounts"],
        "deldata": ["delData", "delete your data from this bot"],
        "seen": ["seen", "mark new users as seen"],
        # "faq":["faq", "more info"],
        "getlocation": ["myLocation", "get a map with current search location"]
    }
}

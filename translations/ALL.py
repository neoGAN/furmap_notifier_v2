ALL = {
    "LANGS":[
        ["EN", "PL"],
        #[  "AUTO"  ],
    ],
    "set_lang_text": "Set language:\nUstaw język:",
    "markdown_url":"<a href=\"{url}\">{text}</a>",
    "markdown_code":"<code>{text}</code>",
    "command_format":"\n<b>/{name}</b> - {description}"
}
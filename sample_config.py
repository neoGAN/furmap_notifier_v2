BOT_TOKEN = "1234567890:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
BOT_NAME = "furmap.net notifier"
BOT_USERNAME = "furmap_notifier_bot"
PARSE_MODE = "MarkdownV2"

DB_URI = "postgresql://YOUR_USER:YOUR_PASSWORD@localhost:5432/furmap_bot"
# DB_ENCODING = None #"utf8" # set to utf-8 in case using PostgreSQL or any other online SQL server probably
SCHEULDER_DB_URI = DB_URI

RUN_DAYS = "mon-fri"
RUN_HOURS = "*"
RUN_MIN_OFFSET = 0

DEBUG = True

SERVER_TIMEOUT = 20
FURMAP_CACHE = "database"  # anything else not implemented
FURMAP_CACHE_LIFESPAN = 86400  # one day
BIO_MAX_LENGTH = 120  # or "DYNAMIC"
MAX_MSG_LEN = 4000  # max 4096

SLEEP_EVERY_ACC = 0.5
SLEEP_EVERY_USER = 1

LOCATION_ACCURACY = 100  # for location widget radius whenever it works

ROUND_DIST = 0

FP_DIST_MARGIN = 4      # distance margin when adding accounts to already seen

MAX_ACCS_PER_REFRESH = 3

FALLBACK_LANG = "EN"

REFRESH_LIMIT = 1
GEO_LIMIT = 5
USER_CHECK_LIMIT = 10

ADMIN_USERNAME = "MAINTAINER_USERNAME"
ADMIN_ID = 1234567

PARSE_MODE = "HTML"

# BOT DEFAULT SETTINGS
DEFAULT_LANG = "en"

DEFAULT_AREA = 50  # km
AREA_LIMIT = 80  # km
DEFAULT_DISTANCE_UNIT = 0  # 0 - km 1 - miles

NO_STATE = -1
LANG_STATE = 2
LOCATION_STATE = 3
AREA_STATE = 4

KNOWN_SERVICES = {
    "telegram": "Telegram",
    "reddit": "Reddit",
    "twitter": "Twitter",
    "vk": "vkontakte",
    "steam": "Steam",
    "playstation": "PSN",
    "xbox": "XBox",
    "nintendo": "Nintendo",
    "skype": "Skype",
    "discord": "Discord",
    "deviantart": "Deviantart",
    "furaff": "Furaffinity",
    "battlenet": "battlenet",
    "instagram": "Instagram",
    "facebook": "F",
    "soundcloud": "Soundcloud"
}

NAME_WHITELIST = [
    "country",
    ["state", "place", "road"],
    ["city", "village", "hamlet"]
]

SETTINGS_LAYOUT = {
    "p0": [  # p0 for page layout, not needed so not implemented yet
        [
            {"t_str": "bot_toggle_btn", "type": "toggle", "name": "bot"},
            {"t_str": "lang_change_btn", "type": "link", "name": "stp_lang"},
        ],
        [
            {"t_str": "location_set_btn", "type": "link", "name": "stp_loca"},
            {"t_str": "area_set_btn", "type": "link", "name": "stp_area"},
            # {"t_str":"dist_unit_btn", "type":"toggle", "name":"unt"}, # not implemented yet
        ],
        [
            {"t_str": "priv_link_btn", "type": "toggle", "name": "lnk"},
        ]
    ]
}

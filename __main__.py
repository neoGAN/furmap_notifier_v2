import importlib
from furmap_notifier_v2.modules import ALL_MODULES
from furmap_notifier_v2 import *

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import (ChatMember, User, Bot, ParseMode)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackQueryHandler)

from furmap_notifier_v2.config import *
import furmap_notifier_v2.modules.functions.translation

for module_name in ALL_MODULES:
    importlib.import_module("furmap_notifier_v2.modules." + module_name)


if __name__ == '__main__':
    updater.start_polling()
    updater.idle()
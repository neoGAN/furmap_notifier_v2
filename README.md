# Furmap notifier v2

Telegram bot made in python using python-telegram-bot wrapper to notify you about new furmap users nearby

## Credit
 - furmap.net for not banning my bot yet
 - bot file structure and some code borrowed from https://github.com/PaulSonOfLars/tgbot
 - jk, this bot uses basically stripped down version of https://github.com/PaulSonOfLars/tgbot
 - nobody else except maybe me ig

# Setup (linux only)
 1. Clone repo ```git clone https://codeberg.org/neoGAN/furmap_notifier_v2```
 2. go to folder ```cd furmap_notifier_v2```
 3. Setup [PostgreSQL](https://github.com/PaulSonOfLars/tgbot#database)/install SQLite
 4. Look through sample_config, set SQLite/PostgreSQL database URI, ADMIN_USERNAME and ADMIN_ID and maybe some other stuff
 5. rename sample_config.py to config.py and customize it, make sure to set [important values](#Important)
 6. create virtual enviroment ```python3 -m venv botenv```
 7. activate virtual enviroment ```source ./botenv/bin/activate```
 8. install requirements ```pip install -r requirements.txt```
 9. run ```python3 -m furmap_notifier_v2```
 10. now, as an admin you should be able to send ```/updatedb``` command and after a minute or two recive ```updated``` as a confirmation that everything works

## On a raspberry pi you might also need to install few packages
```sudo apt install libopenjp2-7 libtiff5```

# Running automatically with systemd
I have provided a sample systemd service file for running this bot in venv on a pi.
To use it make sure you followed the steps in setup correctly and have installed the bot in /home/pi, otherwise adjust the paths inside accordingly
Then soft link or copy the service to /etc/systemd/system:
 1. ```sudo ln -s /home/pi/furmap_notifier_v2/furmap_notifier.service /etc/systemd/system/furmap_notifier.service```
 2. ```sudo systemctl daemon-reload```
 3. ```sudo systemctl enable furmap_notifier.service```
 4. ```sudo systemctl start furmap_notifier.service```

# Important config values to set
 1. ```BOT_TOKEN``` to provide telegram with your bot's API token
 2. ```DB_URI``` to let the bot communicate with DB
 3. ```ADMIN_ID``` to allow you to  use admin commands such as ```/updatedb```
from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.geo import getLocationFromName as getLocation, getCache, compose_location_name
from furmap_notifier_v2.modules.functions.translation import getTranslation, checkLang, getCmds

from furmap_notifier_v2.modules.furCore import checkNewAccounts
import furmap_notifier_v2.modules.sql.users as users
from furmap_notifier_v2.modules.settings import settingsHandler
from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackQueryHandler)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from string import ascii_letters

ignored_chars = ascii_letters+"-"

def start(update, context):
    message = update.effective_message
    user_name = message.chat.first_name
    user = users.user(message.chat_id)
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
    
    keys = [
        [
            InlineKeyboardButton(getTranslation(lang, "lang_change_btn"), callback_data="stp_lang_returnTo=stp=start")
        ],
        #[
        #    InlineKeyboardButton(getTranslation(lang, "open_settings"), callback_data="set_m"),
        #]
    ]
    reply_markup = InlineKeyboardMarkup(keys)
    if(update.callback_query):
        query = update.callback_query
        query.edit_message_text(getTranslation(lang, "bot_start", user_name=user_name, bot_name=BOT_NAME), reply_markup=reply_markup, parse_mode=PARSE_MODE)
        query.answer()
    else:
        message.reply_text(getTranslation(lang, "bot_start", user_name=user_name, bot_name=BOT_NAME), reply_markup=reply_markup, parse_mode=PARSE_MODE)



def setup_language(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)

    query = None
    query_string = ""
    if(update.callback_query):
        query = update.callback_query
        query_string = query.data.split("_")
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
        user.setState(LANG_STATE)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    returnTo = ("" if not("returnTo=" in query.data) else "_returnTo="+"=".join(query.data.split("=")[1:]))

    # compose lang keyboard
    keys = []
    for t_line in getTranslation("ALL", "LANGS", format=False):
        line = []
        for lang in t_line:
            line.append(InlineKeyboardButton(lang, callback_data="stp_sLang_"+lang+returnTo))
        keys.append(line)
    if(returnTo != ""):
        keys.append([InlineKeyboardButton("BACK", callback_data=returnTo[10:].replace("=", "_"))])
    reply_markup = InlineKeyboardMarkup(keys)
    
    if (query):
        query.edit_message_text(getTranslation("ALL", "set_lang_text"), reply_markup=reply_markup, parse_mode=PARSE_MODE)
        query.answer() # "language set!", show_alert=False
    else:
        message.reply_text(getTranslation("ALL", "set_lang_text"), reply_markup=reply_markup, parse_mode=PARSE_MODE)

def set_language(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)

    query = update.callback_query
    query_string = query.data.split("_")[2:]
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
        user.setState(NO_STATE)
    else:
        lang = message.from_user.language_code
        user.create(lang)
    
    language = query_string[0]

    if not (language == "BACK"):
        user.setLanguage(language)
        query.answer(getTranslation(language, "set_lang_successful"), show_alert=False)

    if("returnTo" in query.data):
        returnTo = "_".join(query.data.split("=")[1:])
        
        setupHandler(update, context, overwrite_query_string=returnTo)
        settingsHandler(update, context, overwrite_query_string=returnTo)



def setup_location(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)

    query = None
    query_string = None
    if(update.callback_query):
        query = update.callback_query
        query_string = query.data.split("_")
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    # if the command has parameters, pass update to set_location
    if(not update.callback_query and message.text):
        command = message.text.split(" ")
        if (len(command) > 1):
            set_location(update, context)
            return

    if not user.getAndSetGeoLimit():
        if query:
            query.answer(getTranslation(lang, "location_limit_reached"), show_alert=True)
        else:
            bot.send_message(user.chat_id, getTranslation(lang, "location_limit_reached"))

        return False

    user.setState(LOCATION_STATE)

    if(query):
        returnTo = ("" if not("returnTo=" in query.data) else "_".join(query.data.split("=")[1:]))
        reply_markup = None
        if(returnTo):
            keys = [
                [
                    InlineKeyboardButton(getTranslation(lang, "back"), callback_data=returnTo)
                ]
            ]
            reply_markup = InlineKeyboardMarkup(keys)
        query.edit_message_text(getTranslation(lang, "set_location_info"), reply_markup=reply_markup, parse_mode=PARSE_MODE)
        query.answer()
    else:
        message.reply_text(getTranslation(lang, "set_location_info"), parse_mode=PARSE_MODE)

def set_location(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    if(message.location):
        l = message.location
        user.setLocation((l.latitude, l.longitude), "True")
        message.reply_text(getTranslation(lang, "location_change_ok"), parse_mode=PARSE_MODE)
        checkNewAccounts(message.chat_id, True, FP_DIST_MARGIN)
        user.setState(NO_STATE)
        return

    if(not update.callback_query and message.text):
        command = message.text.replace(":", "").split(" ")
        if(command[0].startswith("/") and len(command)>1):
            location = " ".join(command[1:])
        elif (command[0].startswith("/") and len(command)<2):
            return
        else:
            location = message.text
        if(len(location)<4):
            message.reply_text(getTranslation(lang, "place_error_empty"), parse_mode=PARSE_MODE)
            return
        options = getLocation(location)
        if options == False:
            message.reply_text(getTranslation(lang, "geocache_request_failed"), parse_mode=PARSE_MODE)
            return
        if len(options)<1:
            message.reply_text(getTranslation(lang, "geocache_request_failed"), parse_mode=PARSE_MODE)
            return

        if len(options)<2:
            user.setState(NO_STATE)

            name = compose_location_name(options[0].raw["address"])
            user.setLocation((options[0].lat, options[0].lng), name)
            checkNewAccounts(message.chat_id, True, FP_DIST_MARGIN)

            message.reply_text(getTranslation(lang, "place_data", formatted=name), parse_mode=PARSE_MODE)
            return
        keys = []
        num = 0
        for result in options:
            location_info = result.raw["address"]
            name = compose_location_name(location_info)
            
            keys.append([InlineKeyboardButton(name, callback_data="stp_lcnf_"+str(num))])
            num+=1
    
        reply_markup = InlineKeyboardMarkup(keys)

        message.reply_text(getTranslation(lang, "make_sure_location_correct", location=location), reply_markup=reply_markup, parse_mode=PARSE_MODE)
        user.setState(NO_STATE)



def confirm_location(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    if not(update.callback_query):
        return
    query = update.callback_query
    query_string = query.data.split("_")

    search_index = int(query_string[len(query_string)-1])
    
    search = query.message.text.split("::")[1]

    g = getCache(search)[search_index]
    if not g:
        query.edit_message_text(getTranslation(lang, "data_expired"), parse_mode=PARSE_MODE)
        return

    user.setState(NO_STATE)

    name = compose_location_name(g.raw["address"])
    
    user.setLocation((g.lat, g.lng), name)

    
    query.edit_message_text(getTranslation(lang, "place_data", formatted=name), parse_mode=PARSE_MODE)
    query.answer()

    checkNewAccounts(message.chat_id, True, FP_DIST_MARGIN)



def setup_area(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)

    query = None
    query_string = None
    if(update.callback_query):
        query = update.callback_query
        query_string = query.data.split("_")
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    # if the command has parameters, pass update to set_location
    if(not update.callback_query and message.text):
        command = message.text.split(" ")
        if (len(command) > 1):
            set_area(update, context)
            return

    user.setState(AREA_STATE)

    if(query):
        returnTo = ("" if not("returnTo=" in query.data) else "_".join(query.data.split("=")[1:]))
        reply_markup = None
        if(returnTo):
            keys = [
                [
                    InlineKeyboardButton(getTranslation(lang, "back"), callback_data=returnTo)
                ]
            ]
            reply_markup = InlineKeyboardMarkup(keys)
        query.edit_message_text(getTranslation(lang, "set_area_info"), reply_markup=reply_markup, parse_mode=PARSE_MODE)
        query.answer()
    else:
        message.reply_text(getTranslation(lang, "set_area_info"), parse_mode=PARSE_MODE)

def set_area(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    if(not update.callback_query and message.text):
        command = message.text.split(" ")
        if(command[0].startswith("/") and len(command)>1):
            area = " ".join(command[1:])
        elif (command[0].startswith("/") and len(command)<2):
            return
        else:
            area = message.text

        area_num = area
        for char in ignored_chars:
            area_num = area_num.replace(char, "")

        for char in ".,:":
            area_num = area_num.split(char)[0]

        try:
            area_num = int(area_num)
        except:
            message.reply_text(getTranslation(lang, "unknown_cmd_error"), parse_mode=PARSE_MODE)
            return
        
        if "mil" in area:
            area_num = area_num*1.609344
        elif "m" in area and not "km" in area:
            area_num = area_num*1000

        if area_num > AREA_LIMIT or area_num < 1:
            message.reply_text(getTranslation(lang, "failure_input_setarea_number", AREA_LIMIT=AREA_LIMIT), parse_mode=PARSE_MODE)
            return
        
        user.setArea(area_num)
        user.setState(NO_STATE)

        message.reply_text(getTranslation(lang, "setarea_ok", area=area_num))

        checkNewAccounts(message.chat_id, True, FP_DIST_MARGIN)



def setupHandler(update, context, overwrite_query_string=False):
    message = update.effective_message
    query = update.callback_query
    query_string = query.data[4:].split("_")
    if(overwrite_query_string):
        query_string = overwrite_query_string[4:].split("_")
    options = {
        "start":    start,
        "lang":     setup_language,
        "sLang":    set_language,
        "loca":     setup_location,
        "lcnf":     confirm_location,
        "area":     setup_area,
    }

    if query_string[0] in options:
        options.get(query_string[0])(update, context)


dispatcher.add_handler(CallbackQueryHandler(setupHandler, Filters.chat_type.private, pattern="stp_", run_async=True), group=-5)
dispatcher.add_handler(CommandHandler(getCmds("setlang"), setup_language, Filters.chat_type.private, run_async=True))
dispatcher.add_handler(CommandHandler(getCmds("start"), start, Filters.chat_type.private, run_async=True))
dispatcher.add_handler(CommandHandler(getCmds("setarea"), setup_area, Filters.chat_type.private, run_async=True))
dispatcher.add_handler(CommandHandler(getCmds("setlocation"), setup_location, Filters.chat_type.private, run_async=True))
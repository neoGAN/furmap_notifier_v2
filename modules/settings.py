from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.geo import getLocationFromName as getLocation, getCache, compose_location_name
from furmap_notifier_v2.modules.functions.translation import getTranslation, checkLang, getCmds
import furmap_notifier_v2.modules.sql.users as users
from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackQueryHandler)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

def settings_main(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    warnings = []
    
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)
    
    if not user.getLocation():
        warnings.append(getTranslation(lang, "setup_location"))
    
    user.setState(NO_STATE)
    query = None
    query_string = ""
    if (update.callback_query):
        query = update.callback_query
        query_string = query.data.split("_")
        if(len(query_string)>2):
            if(query_string[2] == "t" and len(query_string)==4): # toggle
                toggles = {
                    "bot":user.toggleBotOn,
                    "lnk":user.togglePrivLinks,
                    "unt":user.toggleDistUnit,
                }
                if(query_string[3] in toggles):
                    result = toggles[query_string[3]]()
                    if type(result) == str:
                        query.answer(getTranslation(lang, result), show_alert=True)
                else:
                    LOGGER.error(f"toggle {query_string[3]} failed, not present")
                    return
            if(query_string[3].startswith("p")):
                # pagination?
                return

    user.update()
    currLoc = user.user.currentLocation
    # render message
    formatVars = {
        **user.user.__dict__,
        "distUnit_verb":user.getDistUnit(),
        "privateLinks_verb":getTranslation(lang, str(user.getPrivLinks())),
        "is_on_verb":getTranslation(lang, "bot"+str(user.getBotOn())),
        "is_not_on_verb":getTranslation(lang, "bot"+str(not user.getBotOn())),
        "user_name":message.from_user.first_name, 
        "bot_name":BOT_NAME,
        "warning":", ".join(warnings),
        "location": getTranslation(lang, "location_not_available") if user.getLocation() and currLoc == "True" else currLoc
    }
    formatVars['language'] = formatVars['lang']
    del formatVars['lang']
    
    #render buttons
    keys = []
    for list1 in SETTINGS_LAYOUT["p0"]:
        row = []
        for item in list1:
            if(item["type"] == "toggle"):
                callback = "set_m_t_"+item["name"]
                text = getTranslation(lang, item["t_str"], **formatVars)
            elif(item["type"] == "link"):
                callback = item["name"]+"_returnTo=set_m"
                text = getTranslation(lang, item["t_str"], **formatVars)
            else:
                LOGGER.error(f"No such type \"{item['type']}\"")
                continue
            row.append(InlineKeyboardButton(text, callback_data=callback))
        keys.append(row)
    reply_markup = InlineKeyboardMarkup(keys)

    if(query):
        query.answer()
        query.edit_message_text(getTranslation(lang, "settings_panel_info", **formatVars), reply_markup=reply_markup, parse_mode=PARSE_MODE)
    else:
        message.reply_text(getTranslation(lang, "settings_panel_info", **formatVars), reply_markup=reply_markup, parse_mode=PARSE_MODE)

def settingsHandler(update, context, overwrite_query_string=False):
    message = update.effective_message
    query = update.callback_query
    query_string = query.data[4:].split("_")
    if(overwrite_query_string):
        query_string = overwrite_query_string[4:].split("_")
    options = {
        "m":settings_main
    }
    if(query_string[0] in options):
        options.get(query_string[0])(update, context)



def myLocation(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    location = user.getLocation()
    
    if not location:
        bot.send_message(user.chat_id, getTranslation(lang, "setup_location"))
        return False

    message.reply_location(location[0], location[1])


dispatcher.add_handler(CallbackQueryHandler(settingsHandler, Filters.chat_type.private, pattern="set_", run_async=True), group=-6)
dispatcher.add_handler(CommandHandler(getCmds("settings"), settings_main, Filters.chat_type.private, run_async=True))

dispatcher.add_handler(CommandHandler(getCmds("getlocation"), myLocation, Filters.chat_type.private, run_async=True))
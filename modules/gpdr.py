from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.translation import getTranslation, getCmds
import furmap_notifier_v2.modules.sql.users as users

from telegram.ext import CommandHandler, CallbackQueryHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto

def gpdrStart(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        message.reply_text(getTranslation(lang, "no_acc_yet"), parse_mode=PARSE_MODE)
        return
    
    keys = [
        [
            InlineKeyboardButton(getTranslation(lang, "False"), callback_data="del_no")
        ],
        [
            InlineKeyboardButton(getTranslation(lang, "False"), callback_data="del_no"),
            InlineKeyboardButton(getTranslation(lang, "True"), callback_data="del_yes"),
            InlineKeyboardButton(getTranslation(lang, "False"), callback_data="del_no"),
        ],
        [
            InlineKeyboardButton(getTranslation(lang, "False"), callback_data="del_no")
        ]
    ]

    reply_markup = InlineKeyboardMarkup(keys)

    message.reply_text(getTranslation(lang, "confirm_deldata"), reply_markup=reply_markup, parse_mode=PARSE_MODE)

def gpdrConfirm(update, context):
    message = update.effective_message

    if update.callback_query:
        query = update.callback_query
        query_data = query.data.split("_")
        user = users.user(message.chat_id)
        if(user.exists()):
            lang = user.getLanguage(message.from_user.language_code)
        else:
            lang = message.from_user.language_code
            query.edit_message_text(getTranslation(lang, "no_acc_yet"), parse_mode=PARSE_MODE)
            return

        if query_data[1] == "yes":
            user.delete()
            query.answer(getTranslation(lang, "deldata_done"), show_alert=True)
        else:            
            query.answer(getTranslation(lang, "action_reset"), show_alert=True)
            query.delete_message()
                
dispatcher.add_handler(CommandHandler(getCmds("deldata"), gpdrStart, Filters.chat_type.private, run_async=True))

dispatcher.add_handler(CallbackQueryHandler(gpdrConfirm, Filters.chat_type.private, pattern="del_", run_async=False))
from PIL import Image, ImageDraw, ImageFont
from string import ascii_letters, digits
import json
import os
current = os.path.dirname(__file__)
logodir = os.path.join(current, "logos")
print(current, logodir)
font_path = os.path.join(current, "./Roboto-Thin.ttf")

class card:
    cornermask = Image.open(os.path.join(current, "mask.png"))
    default_pfp = Image.open(os.path.join(current, "./default.png"))
    icons = {

    }
    fnt = ImageFont.truetype(font_path, 32)
    big_fnt = ImageFont.truetype(font_path, 36)
    smaller_fnt = ImageFont.truetype(font_path, 26)
    nano_fnt = ImageFont.truetype(font_path, 20)
    pico_fnt = ImageFont.truetype(font_path, 16)

    def __init__(self):
        folder = os.path.join(current, "logos")
        for icon in [each for each in os.listdir(folder) if each.endswith('.png')]:
            iconpath = os.path.join(logodir, icon)
            print(icon[:-4])
            with open(iconpath[:-3]+"json") as f:
                data = json.loads(f.read())
            self.icons[icon[:-4]] = {
                "img":Image.open(iconpath).convert("RGBA").resize((30, 30)),
                "color":data["color"]
            }

    def generate(self, image, text, accounts={}, n=13):
        if not image:
            image = self.default_pfp
        if accounts['hue']:
            color = f"hsl({accounts['hue']}, 100%, 90%)"
        else:
            color = "rgb(255, 255, 255)"
        new = Image.new("RGBA", (200, 200), color)
        final = Image.new("RGBA", (450, 240), color)
        text = text.replace("&amp;", "&").replace("&lt;", "<").replace("&gt;", ">")
        
        if(image.size[0] != 200):
            raise Exception("image horizontal size has to be 200")
        if image.size[1] > 200:
            image = image.crop((0, int(image.size[1]/2-100), 200, int(image.size[1]/2+100)))
            
        if image.size[1] < 200:
            _image = Image.new("RGBA", (200, 200), (255, 255, 255))
            _image.paste(image, (0, int(100-image.size[1]/2)), image)
            image = _image
            del _image
        
        new.paste(image, (0, int(image.size[1]/2-100)*-1), self.cornermask)
        d = ImageDraw.Draw(final)
        font = self.fnt
        splitted_text = self.splitTestBasedonProfile(d, text, font, 195)
        if sum('\n' in s for s in splitted_text)>0:
            font = self.smaller_fnt
            splitted_text = self.splitTestBasedonProfile(d, text, font, 195)
        if sum('\n' in s for s in splitted_text)>1:
            font = self.nano_fnt
            splitted_text = self.splitTestBasedonProfile(d, text, font, 195)
        if sum('\n' in s for s in splitted_text)>1:
            font = self.pico_fnt
            splitted_text = self.splitTestBasedonProfile(d, text, font, 195)
        d.multiline_text((220,10), splitted_text, font=font, fill=(0, 0, 0)) #"-\n".join([text[i:i+n] for i in range(0, len(text), n)])
        final.paste(new, (10,int(final.size[1]/2-100)))

        accsY = 0
        spacing = 33
        spacefromtitle = 70
        for account in accounts.keys():
            if accounts[account] and account in self.icons:
                icon = self.icons[account]["img"]
                txtcolor = self.icons[account]["color"]
                final.paste(icon, (220, spacefromtitle+accsY), icon)
                width = 180
                text = accounts[account].replace("&amp;", "&").replace("&lt;", "<").replace("&gt;", ">")
                font = self.smaller_fnt
                splitted_text = self.splitTestBasedonProfile(d, text, font, width)
                if sum('\n' in s for s in splitted_text)>0:
                    font = self.nano_fnt
                    splitted_text = self.splitTestBasedonProfile(d, text, font, width)
                if sum('\n' in s for s in splitted_text)>0:
                    font = self.pico_fnt
                    splitted_text = self.splitTestBasedonProfile(d, text, font, width)
                yfontsize = d.textsize("A", font=font)[1]
                d.multiline_text((222+icon.size[0], spacefromtitle+accsY), splitted_text, font=font, fill=tuple(txtcolor))
                accsY += spacing

        return final
    
    def splitTestBasedonProfile(self, instance, text:str, font, maxsize, split="-\n", spacesplit="\n"):
        result = ""
        currentSize = instance.textsize(text[0], font=font)[0]*-1 #font.getSize(char)
        #maxsize -= font.getSize(split.replace("\n", "")) # save some space for -
        for char in text:
            currentSize += instance.textsize(char, font=font)[0] #font.getSize(char)
            #result += char
            if currentSize >= maxsize and char != " " and result[-1:] != " ":
                result += split
                currentSize = 0
            elif currentSize >= maxsize:
                result += spacesplit
                currentSize = 0
            result += char
        return result
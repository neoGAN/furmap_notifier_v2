telegramurl = lambda x : "https://t.me/"+str(x).replace("@", "")

profileurl = lambda x : "https://furmap.net/profile/"+x


knownservices = {
    "telegram":"Telegram",
    "reddit":"Reddit",
    "twitter":"Twitter",
    "vk":"vkontakte",
    "steam":"Steam",
    "playstation":"PSN",
    "xbox":"XBox",
    "nintendo":"Nintendo",
    "skype":"Skype",
    "discord":"Discord",
    "deviantart":"Deviantart",
    "furaff":"Furaffinity",
    "battlenet":"battlenet",
    "instagram":"Ig",
    "facebook":"Fb",
}

nitterurl = lambda x : "https://twiiit.com/"+str(x).replace("@", "")
libredditurl = lambda x : "https://libredd.it/"+("" if ("u/" in x) else "u/")+str(x)
bibliogramurl = lambda x : "https://bibliogram.art/u/"+str(x).replace("@", "")

knownPrivUrls = {
    "telegram":telegramurl,
    "reddit":libredditurl,
    "twitter":nitterurl,
    "instagram":bibliogramurl
}


twitterurl = lambda x : "https://twitter.com/"+str(x).replace("@", "")
redditurl = lambda x : "https://reddit.com/"+("" if (x.startswith("u/")) or (x.startswith("r/")) else "u/")+str(x)
igurl = lambda x : "https://instagram.com/u/"+str(x).replace("@", "")
furaffurl = lambda x : "https://www.furaffinity.net/user/"+str(x)
daurl = lambda x : "https://www.deviantart.com/"+str(x)
steamurl = lambda x : "https://steamcommunity.com/search/users/#text="+str(x)
fburl = lambda x : "https://facebook.com/"+str(x).replace("facebook.com/", "")
scurl = lambda x : "https://soundcloud.com/"+str(x)

knownUrls = {
    "furmap":profileurl,
    "telegram":telegramurl,
    "reddit":redditurl,
    "twitter":twitterurl,
    "instagram":igurl,
    "furaff":furaffurl,
    "deviantart":daurl,
    "steam":steamurl,
    "facebook":fburl,
    "soundcloud":scurl,
}

def getLink(serviceName:str, name:str, prioritizePrivate:bool=True):
    if prioritizePrivate:
        if serviceName in knownPrivUrls:
            return knownPrivUrls[serviceName](name)
        elif serviceName in knownUrls:
            return knownUrls[serviceName](name)
    else:
        if serviceName in knownUrls:
            return knownUrls[serviceName](name)
        elif serviceName in knownPrivUrls:
            return knownPrivUrls[serviceName](name)
    return False
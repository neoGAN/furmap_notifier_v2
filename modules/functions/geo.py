import geocoder
import json

from time import time, sleep
from furmap_notifier_v2.config import NAME_WHITELIST

cache = {

}
global last_query
last_query = 0

def getLocationFromName(name:str):
    if (name.lower() in cache):
        return cache
    global last_query
    if(time() - last_query < 1):
        sleep(1 - (time() - last_query))
    g = geocoder.osm(name, maxRows=4)
    cache[name.lower()] = g
    last_query = time()
    return g

def getCache(name:str):
    if not (name.lower() in cache):
        return False
    return cache[name.lower()]

def compose_location_name(location_info):
    name = ""
    for w in NAME_WHITELIST:
        if type(w) == list:
            for addr_name in w:
                if(addr_name in location_info):
                    print("using "+addr_name)
                    if len(name)>0:
                        name += ", "
                    name += location_info[addr_name]
                    break
        else:
            if(w in location_info):
                if len(name)>0:
                    name += ", "
                print("using "+w)
                name += location_info[w]
    return name
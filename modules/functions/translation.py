from furmap_notifier_v2.translations import ALL_MODULES
from furmap_notifier_v2.config import FALLBACK_LANG, ADMIN_USERNAME
#from os.path import basename
import importlib
from string import ascii_letters, digits

translations = {}

for module_name in ALL_MODULES:
    translation = importlib.import_module("furmap_notifier_v2.translations." + module_name)
    translations[module_name.upper()] = translation.__dict__[module_name.upper()]
    del translation

def getTranslation(lang:str, _name:str, *args, **kwargs):
    lang = lang.upper()
    if not (lang in translations):
        lang = FALLBACK_LANG.upper()
    if not (_name in translations[lang]):
        to_format = translations[lang]["lang_error"]
    else:
        to_format = translations[lang][_name]
    if not (kwargs.pop("format", True)):
        return to_format
    return to_format.format(*args, **kwargs)

def getCmds(command:str):
    #it's only running at the start of the bot so speed is not a concern
    cmdList = []
    for key in translations.keys():
        if not "CMDS" in translations[key]:
            continue
        commands = translations[key]["CMDS"]
        if command in commands:
            cmdList.append(commands[command][0])
    return cmdList

def getAllCmds(lang):
    if lang in translations.keys():
        if not "CMDS" in translations[lang]:
            return False
        return translations[lang]["CMDS"].values()

def checkLang(lang:str):
    lang = lang.upper()
    return lang in translations

def sanitize(string:str):
    return string.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
from urllib import parse, request
from geopy.distance import great_circle as geodistance
import json
from time import time

import logging

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO)

LOGGER = logging.getLogger(__name__)

class furmap:
    mapJSON = "https://hg7h7jw5wc.execute-api.us-east-1.amazonaws.com/dev/map"
    profileURL = lambda self, x : "https://furmap.net/profile/"+str(x)
    profileJSON = lambda self, x : "https://hg7h7jw5wc.execute-api.us-east-1.amazonaws.com/dev/user/"+str(x)
    imgurl = lambda self, x : "https://furmap.us-east-1.linodeobjects.com/users/high/"+parse.quote(x)+".png"
    database = False
    pos2km = 0.008999
    pos2mil = 0.01448
    cacheExpire = 86400 # seconds

    def __init__(self, debug=False, requestTimeout=20):
        self.debug = debug
        self.timeout = requestTimeout
        return

    def update(self):
        LOGGER.info("Quering furmap.net JSON map")
        try:
            response = request.urlopen(self.mapJSON, timeout=self.timeout)
        except:
            LOGGER.error("furmap not reachable")
            return False
        if not (response):
            LOGGER.error("urllib handler didn't handle the request?")
            return False
        if not (response.status >= 200 and response.status < 300):
            LOGGER.error(f"server returned {response.status}")
            return False
        
        # parse to JSON
        LOGGER.info("map seems to be fine, parsing JSON")
        mapData = json.loads(response.read())
        LOGGER.info("JSON parsed, writing to DB")
        self.database.bulkSet(full=False, userList=mapData)
        #length = len(mapData)
        #currentLen=0
        #for user in mapData:
        #    self.database.set(full=False, **user)
        #    LOGGER.info(f"{currentLen}/{length}")
        #    currentLen += 1
        LOGGER.info("updated furmap users")
            
    
    def checkAccountsIn(self, location:(float, float), area, distanceUnit="km", detailedCheck=True):
        if (self.database):
            start = time()
            area_pos = area*(self.pos2km if distanceUnit=="km" else self.pos2mil)
            usersInSqrArea = self.database.searchArea(location, area_pos)
            end1 = time()
            if self.debug:
                print(f"Time spent on searching: {end1-start} seconds")
            
            if(not detailedCheck):
                return usersInSqrArea
            
            usersInRoundArea = []
            for user in usersInSqrArea:
                thatUser = (user['lat'], user['lon'])
                distance = geodistance(location, thatUser)
                if(distanceUnit=="km"):
                    distance = distance.km
                else:
                    distance = distance.miles
                
                if(distance <= area):
                    usersInRoundArea.append({
                        **user,
                        "d": distance
                    })
            end2 = time()
            if self.debug:
                print(f"Total time: {end2-start} seconds")
            return usersInRoundArea

    def getUserInfo(self, user_id:str, require_full=True):
        if not (user_id):
            return False
        
        user = self.database.get(user_id)
        load = True
        if user:
            if (not require_full or user.full) and user.lastFullUpdate+self.cacheExpire>time():
                load = False
                #return json.loads(user.user.other)
        if load:
            try:
                response = request.urlopen(self.profileJSON(user_id), timeout=self.timeout)
            except:
                LOGGER.error("furmap not reachable")
                return False
            if not (response):
                LOGGER.error("urllib handler didn't handle the request?")
                return False
            if not (response.status >= 200 and response.status < 300):
                LOGGER.error(f"server returned {response.status}")
                return False
            user = json.loads(response.read().decode("utf-8"))
            if type(user) == list:
                user = user[0]
            self.database.set(full=True, **user)
        else:
            if type(user) == list:
                user = user[0]
            user = json.loads(user.other)
            #jsonData.update({**user.__dict__})
            #user = jsonData
        
        return self.database._translateFurmap(user, False)

    def getUserImage(self, user_id:str):
        # get profile picture
        try:
            response = request.urlopen(self.imgurl(user_id), timeout=self.timeout)
        except:
            LOGGER.error("pfp endpoint not reachable")
            return False
        if not (response):
            LOGGER.error("urllib handler didn't handle the request?")
            return False
        if not (response.status >= 200 and response.status < 300):
            LOGGER.error(f"server returned {response.status}")
            return False
        
        return response
        
from pytz import utc
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor

from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *

from furmap_notifier_v2.modules.furCore import update_anc_check
from furmap_notifier_v2.modules.sql.users import resetLimits

# scheulder setup
jobstores = {
    'default': SQLAlchemyJobStore(url=SCHEULDER_DB_URI)
}

executors = {
    'default': ThreadPoolExecutor(5),
    'processpool': ProcessPoolExecutor(2)
}

job_defaults = {
    'coalesce': False,
    'max_instances': 2
}

global scheduler

scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults, timezone=utc)
scheduler.start()



if len(scheduler.get_jobs(jobstore='default')) < 1:
    scheduler.add_job(update_anc_check, trigger='cron', hour=RUN_HOURS, minute=RUN_MIN_OFFSET, id='DBcheck')
    scheduler.add_job(resetLimits, trigger='cron', hour=RUN_HOURS, minute=RUN_MIN_OFFSET, id='ResetLimits')
else:
    scheduler.reschedule_job('DBcheck', trigger='cron', hour=RUN_HOURS, minute=RUN_MIN_OFFSET)
    scheduler.reschedule_job('ResetLimits', trigger='cron', hour=RUN_HOURS, minute=RUN_MIN_OFFSET)
from telegram.ext import Filters, CommandHandler
from furmap_notifier_v2.modules.functions.translation import getTranslation, getAllCmds, getCmds, checkLang
import furmap_notifier_v2.modules.sql.users as users

from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *

def help(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)

    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code if checkLang(message.from_user.language_code) else FALLBACK_LANG

    cmds = getAllCmds(lang)
    commands = ""

    for cmd in cmds:
        commands += getTranslation("ALL", "command_format", name=cmd[0], description=cmd[1])

    message.reply_text(getTranslation(lang, "bot_help", commands=commands), parse_mode=PARSE_MODE)
        
dispatcher.add_handler(CommandHandler(getCmds("help"), help, Filters.chat_type.private, run_async=True))
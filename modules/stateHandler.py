from telegram.ext import Filters, MessageHandler
from furmap_notifier_v2.modules.functions.translation import getTranslation
import furmap_notifier_v2.modules.sql.users as users

from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *

from furmap_notifier_v2.modules.setup import set_location, set_area

stateDist = {
    LOCATION_STATE: set_location,
    AREA_STATE: set_area,
}

def getResponse(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    if not (user.exists()):
        return
    if(int(user.getState()) in stateDist):
        stateDist[int(user.getState())](update, context)

dispatcher.add_handler(MessageHandler(Filters.chat_type.private, getResponse, run_async=True), group=-10)
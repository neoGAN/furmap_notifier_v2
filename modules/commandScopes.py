from telegram import BotCommand, BotCommandScope, BotCommandScopeAllPrivateChats
from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.translation import getAllCmds, translations

for lang in translations.keys():
    if lang == "ALL":
        continue
    cmds = getAllCmds(lang)
    if cmds:
        botCmds = []
        for cmd in cmds:
            botCmds.append(
                BotCommand(cmd[0].lower(), cmd[1])
            )
        LOGGER.info(f"Setting commands for {lang}")
        bot.set_my_commands(botCmds, language_code=lang.lower())
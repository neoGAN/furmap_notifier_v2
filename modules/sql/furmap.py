from furmap_notifier_v2.modules.sql import BASE, SESSION
from sqlalchemy import Column, UnicodeText, Boolean, Integer, Float, String, BigInteger, JSON, func
import json
import threading
from time import time
from math import acos, cos, sin, radians, degrees
import hashlib

class furmapUsers(BASE):
    __tablename__ = "furmapUsers"

    user_id         =   Column(String(14), primary_key=True)
    name            =   Column(String)
    hue             =   Column(Integer)
    bio             =   Column(String)

    lat             =   Column(Float)
    lon             =   Column(Float)
    
    telegram        =   Column(String)
    discord         =   Column(String)
    twitter         =   Column(String)
    furaff          =   Column(String)
    deviantart      =   Column(String)
    other           =   Column(JSON)
    full            =   Column(Boolean, default=False)

    knownIDs        =   Column(String)

    lastUpdate      =   Column(Integer)
    lastFullUpdate  =   Column(Integer)

    profileMakeDate =   Column(String)
    profileModDate  =   Column(String)

    dataHash        =   Column(String)

furmapUsers.__table__.create(checkfirst=True)
INSERTION_LOCK = threading.RLock()

class furmapDB:
    furmap2DB = {
        "idsmall":"user_id",
        "user_id":"user_id",

        "i":"user_id",
        "a":"lat",
        "o":"lon",
        "u":"name",

        "description":"bio",
        "latitude":"lat",
        "longitude":"lon",
        "telegram":"telegram",
        "discord":"discord",
        "twitter":"twitter",
        "furaff":"furaff",
        "deviantart":"deviantart",
        "user_id":"user_id",
        "username":"name",
        "profileColor":"hue",
        "creationDate":"profileMakeDate",
        "modificationDate":"profileModDate"
    }
    def get(self, user_id:str):
        return SESSION.query(furmapUsers).get(user_id)

    def searchName(self, string:str):
        results = []
        # no point in doing any iterations
        results += SESSION.query(furmapUsers).filter(furmapUsers.telegram.like(string)).all()
        results += SESSION.query(furmapUsers).filter(furmapUsers.discord.like(string)).all()
        results += SESSION.query(furmapUsers).filter(furmapUsers.twitter.like(string)).all()
        results += SESSION.query(furmapUsers).filter(furmapUsers.furaff.like(string)).all()
        results += SESSION.query(furmapUsers).filter(furmapUsers.deviantart.like(string)).all()
        results += SESSION.query(furmapUsers).filter(furmapUsers.name.like(string)).all()

        noCloneResults = []
        for result in results:
            if not (result in noCloneResults):
                noCloneResults.append(result)

        return noCloneResults

    def searchArea(self, location:(float, float), degArea:float, method = 0):
        if method == 0:
            radArea = radians(degArea)
            results = SESSION.query(furmapUsers).filter(
                func.acos(
                    func.cos ( func.radians(location[0]) )
                    * func.cos( func.radians( furmapUsers.lat ) )
                    * func.cos( func.radians( furmapUsers.lon ) - func.radians(location[1]) )
                    + func.sin ( func.radians(location[0]) )
                    * func.sin( func.radians( furmapUsers.lat ) )
                ).label("dist") < radArea
            )
        else:
            results = SESSION.query(furmapUsers).filter(
                                                            furmapUsers.lat>location[0]-degArea, 
                                                            furmapUsers.lon>location[1]-degArea
                                                        ).all()
        return results


    def set(self, full:bool, user_id=False, returnDict=False, **kwargs):
        DBvals = self._translateFurmap(kwargs.copy())

        if user_id:
            DBvals["user_id"] = user_id
        DBvals["full"] = full
        DBvals["lastUpdate"] = time()
        if full:
            DBvals["lastFullUpdate"] = time()
            DBvals["other"] = json.dumps(kwargs)

        if "profileModDate" in DBvals:
            DBvals["dataHash"] = hashlib.shake_128(str(DBvals["profileModDate"]).encode("UTF-8")).hexdigest(8)
        
        if not returnDict:
            with INSERTION_LOCK:
                user = SESSION.query(furmapUsers).get(DBvals["user_id"])
                if user:
                    for kwarg in DBvals.keys():
                        setattr(user, kwarg, DBvals.get(kwarg))
                else:
                    SESSION.add(furmapUsers(**DBvals))
                SESSION.commit()
            return True
        else:
            return DBvals

    def bulkSet(self, full:bool, userList):
        with INSERTION_LOCK:
            for num in range(len(userList)):
                DBvals = self.set(full=full, returnDict=True, **userList[num])
                user = SESSION.query(furmapUsers).get(DBvals["user_id"])
                if user:
                    for kwarg in DBvals.keys():
                        setattr(user, kwarg, DBvals.get(kwarg))
                else:
                    SESSION.add(furmapUsers(**DBvals))
            SESSION.commit()


    def _translateFurmap(self, args:dict, delete=True):
        keys = list(args.keys())
        for key in keys:
            if key in self.furmap2DB:
                args[self.furmap2DB[key]] = args[key]
                if key != self.furmap2DB[key] and delete:
                    del args[key]
            elif delete:
                del args[key]
        return args
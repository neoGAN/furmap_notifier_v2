from furmap_notifier_v2.modules.sql import BASE, SESSION
from sqlalchemy import Column, UnicodeText, Boolean, Integer, Float, String, BigInteger
from random import randint
import threading

from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.translation import checkLang

class users(BASE):
    __tablename__ = "users"

    #id = Column(BigInteger().with_variant(Integer, "sqlite"), primary_key=True)
    chat_id         =   Column(String(20), primary_key=True)
    state           =   Column(Integer, default=-1)
    is_on           =   Column(Boolean, default=False)
    to_delete       =   Column(Boolean, default=False)
    latitude        =   Column(Float, default=0)
    longitude       =   Column(Float, default=0)
    area            =   Column(Integer, default=DEFAULT_AREA)
    distUnit        =   Column(Integer, default=DEFAULT_DISTANCE_UNIT) # 0 = km 1 = miles
    lang            =   Column(String(2), default=DEFAULT_LANG)
    overwriteLang   =   Column(Boolean, default=False)
    dbsTries        =   Column(Integer, default=0)
    geoTries        =   Column(Integer, default=0)
    userCheckTries  =   Column(Integer, default=0)
    sendFails       =   Column(Integer, default=0)
    canBroadcast    =   Column(Boolean, default=True)
    privateLinks    =   Column(Boolean, default=True)
    currentLocation =   Column(String(50), default="")

    knownIDs        =   Column(String, default="")

users.__table__.create(checkfirst=True)
INSERTION_LOCK = threading.RLock()

def RandomizeLocation(location, x=False):
    r = randint(40, 120)/200
    e = randint(-1,1)
    if e == 0: e = 1
    print(r)
    print((r*0.0001+1))
    return min(max(abs(location*(r*0.0001+e)), -180), 180)

def getUser(chat_id:str):
    response = SESSION.query(users).get(chat_id)
    if not response:
        return False
    return response

def getLanguage(chat_id:str, chat_lang:str=DEFAULT_LANG):
    response = SESSION.query(users).get(chat_id)
    if not (response):
        return chat_lang.upper()
    if not (response.overwriteLang):
        return chat_lang.upper()
    return response.lang.upper()

def setLanguage(chat_id:str, lang:str):
    response = SESSION.query(users).get(chat_id)
    with INSERTION_LOCK:
        if(lang == "AUTO"):
            response.overwriteLang = False
        elif(checkLang(lang)):
            response.overwriteLang = True
            response.lang = lang.upper()
        else:
            return False
    return True

def setState(chat_id:str, state:Integer):
    response = SESSION.query(users).get(chat_id)
    with INSERTION_LOCK:
        response.state = state
        SESSION.commit()
    return response

def getState(chat_id:str):
    response = SESSION.query(users).get(chat_id)
    if(response):
        return response.state
    return -1

def newUser(chat_id:str, lang="en", **kwargs):
    user = SESSION.query(users).get(str(chat_id))
    if not (user):
        with INSERTION_LOCK:
            SESSION.add(users(chat_id=chat_id, lang=lang, **kwargs))
            SESSION.commit()

def getUsersOn():
    response = SESSION.query(users).filter((users.is_on == True) and (users.to_delete != True) ).all()
    return response

def resetLimits():
    with INSERTION_LOCK:
        SESSION.query(users).update({
            users.dbsTries: 0,
            users.geoTries: 0,
            users.userCheckTries: 0
        })
        SESSION.commit()
    return True
    
class user:
    def __init__(self, chat_id):
        if type(chat_id) == int:
            chat_id = str(chat_id)
        
        if type(chat_id) == str:
            self.chat_id = chat_id
            self.user = SESSION.query(users).get(chat_id)
        else:
            self.chat_id = chat_id.chat_id
            self.user = chat_id
    
    def update(self):
        self.user = SESSION.query(users).get(self.chat_id)

    def exists(self):
        if self.user != None:
            return not self.user.to_delete
        else:
            return False

    def create(self, lang="en", **kwargs):
        if not (self.user):
            with INSERTION_LOCK:
                SESSION.add(users(chat_id=self.chat_id, lang=lang, **kwargs))
                SESSION.commit()
                self.user = SESSION.query(users).get(str(self.chat_id))
            return True
        else:
            with INSERTION_LOCK:
                self.user.to_delete = False
                SESSION.commit()
            return False
    
    def setState(self, state:Integer):
        with INSERTION_LOCK:
            self.user.state = state
            SESSION.commit()

    def getState(self):
        return self.user.state
    
    def setLanguage(self, lang:str):
        with INSERTION_LOCK:
            if(lang == "AUTO"):
                self.user.overwriteLang = False
            elif(checkLang(lang)):
                self.user.overwriteLang = True
                self.user.lang = lang.upper()
            else:
                return False
            SESSION.commit()
        return True
    
    def getLanguage(self, chat_lang=None):
        if not (chat_lang):
            chat_lang = DEFAULT_LANG
        if not (self.user):
            return chat_lang.upper()
        if not (self.user.overwriteLang):
            return chat_lang.upper()
        return self.user.lang.upper()

    def setLocation(self, location:(float, float), name=None):
        with INSERTION_LOCK:
            self.user.latitude = RandomizeLocation(location[0])
            self.user.longitude = RandomizeLocation(location[1])
            self.user.currentLocation = name

            SESSION.commit()
    
    def getLocation(self):
        if (self.user.latitude) and (self.user.longitude):
            return (self.user.latitude, self.user.longitude)
        else:
            return False

    def getArea(self):
        return self.user.area

    def setArea(self, area:int):
        if(area>0 and area <= AREA_LIMIT):
            self.user.area = area # TODO: take strings and recognize miles and kilometers
            return True
        else:
            return False
    
    def getDistUnit(self, num:bool=False):
        if (num):
            return self.user.distUnit
        else:
            return ("km" if self.user.distUnit==0 else "miles")

    def setDistUnit(self, unit:str):
        unit = unit.replace(" ", "")
        with INSERTION_LOCK:
            try:
                unit = int(unit)
                if(unit>=0 and unit<2):
                    user.distUnit = unit
            except:
                if(unit.startswith("k")):
                    user.distUnit = 0
                else:
                    user.distUnit = 1
            SESSION.commit()
        return user.DistUnit

    def toggleDistUnit(self, num:bool=False):
        unit = self.getDistUnit(True)
        newUnit = abs(unit-1)
        with INSERTION_LOCK:
            self.user.distUnit = newUnit
            SESSION.commit()
        if (num):
            return newUnit
        else:
            return ("km" if newUnit==0 else "miles")

    def getPrivLinks(self):
        return self.user.privateLinks

    def setPrivLinks(self, state:bool):
        with INSERTION_LOCK:
            self.user.privateLinks = state
            SESSION.commit()
    
    def togglePrivLinks(self):
        with INSERTION_LOCK:
            self.user.privateLinks = not self.user.privateLinks
            SESSION.commit()
        return self.user.privateLinks

    def getBotOn(self):
        return self.user.is_on

    def setBotOn(self, state:bool):
        if not self.user.latitude and not self.user.longitude:
            return "setup_location"
        with INSERTION_LOCK:
            self.user.is_on = state
            SESSION.commit()
            return True

    def getKnownIDs(self):
        knownIDs = self.user.knownIDs
        if not knownIDs:
            return []
        else:
            return self.user.knownIDs.split("-")

    def addID(self, id:str):
        with INSERTION_LOCK:
            self.user.knownIDs += "-"+id
            SESSION.commit()
    
    def toggleBotOn(self):
        if not self.user.latitude and not self.user.longitude:
            return "setup_location"
        with INSERTION_LOCK:
            self.user.is_on = not self.user.is_on
            SESSION.commit()
        return self.user.is_on

    def getSendFails(self):
        return self.user.sendFails

    def addSendFail(self):
        with INSERTION_LOCK:
            self.user.sendFails += 1
            SESSION.commit()
    
    def resetSendFails(self):
        with INSERTION_LOCK:
            self.user.sendFails = 0
            SESSION.commit()

    def getAndSetRefreshLimit(self):
        with INSERTION_LOCK:
            self.user.dbsTries += 1
            SESSION.commit()
            return (self.user.dbsTries <= REFRESH_LIMIT)

    def getAndSetGeoLimit(self):
        with INSERTION_LOCK:
            self.user.geoTries += 1
            SESSION.commit()
            return (self.user.geoTries <= GEO_LIMIT)

    def getAndSetUserCheckLimit(self):
        with INSERTION_LOCK:
            self.user.userCheckTries += 1
            SESSION.commit()
            return (self.user.userCheckTries <= USER_CHECK_LIMIT)
    def delete(self):
        with INSERTION_LOCK:
            self.user.to_delete = True
            SESSION.commit()

    def sync(self):
        with INSERTION_LOCK:
            SESSION.commit()
    
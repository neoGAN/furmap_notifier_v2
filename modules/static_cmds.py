from telegram.ext import Filters, CommandHandler
from furmap_notifier_v2.modules.functions.translation import getTranslation, getCmds
import furmap_notifier_v2.modules.sql.users as users

from furmap_notifier_v2 import *
from furmap_notifier_v2.config import PARSE_MODE, BOT_NAME

staticCmds = {
    "tos": ["tos",                  Filters.chat_type.private],
    "enable":  ["deprecated_cmd",       Filters.chat_type.private],
    "disable": ["deprecated_cmd",      Filters.chat_type.private],
    "listaccounts": ["deprecated_cmd", Filters.chat_type.private],
}


def runStaticCmd(update, context):
    message = update.effective_message
    command = message.text[1:].split(" ")

    user = users.user(message.chat_id)
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
    if command[0] in staticCmds:
        cmd = staticCmds[command[0]]
        message.reply_text(getTranslation(
            lang, cmd[0]), parse_mode=PARSE_MODE)
        return


LOGGER.info("Static commands to load: " +
            str([key for key in staticCmds.keys()]))
for key in staticCmds.keys():
    cmd = staticCmds[key]
    dispatcher.add_handler(CommandHandler(
        getCmds(key), runStaticCmd, cmd[1], run_async=True))

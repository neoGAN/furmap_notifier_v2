from furmap_notifier_v2 import *
from furmap_notifier_v2.config import NO_STATE, PARSE_MODE
from furmap_notifier_v2.modules.functions.translation import getTranslation
import furmap_notifier_v2.modules.sql.users as users
from telegram.ext import CommandHandler, Filters, DispatcherHandlerStop

def resetState(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    
    if(user):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)
    
    user.setState(NO_STATE)
    message.reply_text(getTranslation(lang, "action_reset"), parse_mode=PARSE_MODE)
    raise DispatcherHandlerStop()

dispatcher.add_handler(CommandHandler("cancel", resetState))
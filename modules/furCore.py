from furmap_notifier_v2 import *
from furmap_notifier_v2.config import *
from furmap_notifier_v2.modules.functions.translation import getTranslation, getCmds, sanitize

from telegram.ext import CommandHandler, CallbackQueryHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto

from geopy.distance import great_circle as geodistance

import furmap_notifier_v2.modules.sql.users as users
import furmap_notifier_v2.modules.sql.furmap as sql
import furmap_notifier_v2.modules.functions.furmap as engine
import furmap_notifier_v2.modules.functions.cardGen.cardGen as cardGen
from furmap_notifier_v2.modules.functions.linkGetter import getLink

from PIL import Image
from urllib.request import urlopen
from math import floor
from time import sleep
import io

furmap = engine.furmap(DEBUG, SERVER_TIMEOUT)
furmap.database = sql.furmapDB()
furmap.cacheExpire = FURMAP_CACHE_LIFESPAN

card = cardGen.card().generate


def checkNewAccounts(chat_id, addNewIDs=True, margin=0):
    if type(chat_id) == users.user:
        user = chat_id
    else:
        user = users.user(chat_id)

    location = user.getLocation()
    area = user.getArea()+margin
    unit = user.getDistUnit()

    knownIDs = user.getKnownIDs()

    if not location:
        return False

    accounts = furmap.checkAccountsIn(location, area, unit, False)

    unknownIDs = []
    for account in accounts:
        if not (account.user_id in knownIDs):
            distance = geodistance(location, (account.lat, account.lon))
            if(unit == "km"):
                distance = distance.km
            else:
                distance = distance.miles

            if(distance <= area):
                unknownIDs.append({
                    **account.__dict__,
                    "distance": distance
                })
                if addNewIDs:
                    user.addID(account.user_id)

    return unknownIDs


def generateUserInfo(user, distance: int, distUnit: str, lang, prioritizePrivate=True):
    if type(user) == str:
        user = furmap.getUserInfo(user, True)
    elif type(user) != dict:
        return False

    user_id = user["user_id"]
    # generate social media links
    socialMedia = []
    print(user)
    for key in user.keys():
        if key in KNOWN_SERVICES and user[key]:
            url = getLink(key, user[key], prioritizePrivate)
            if url:
                socialMedia.append(
                    f"{KNOWN_SERVICES[key]}: {getTranslation('ALL', 'markdown_url', url=url, text=sanitize(user[key]))}")
            else:
                socialMedia.append(
                    f"{KNOWN_SERVICES[key]}: {getTranslation('ALL', 'markdown_code', text=sanitize(user[key]))}")

    if user["bio"]:
        user["bio"] = sanitize(user["bio"])

    user["name"] = sanitize(user["name"])

    # add description if any
    if user["bio"] and type(BIO_MAX_LENGTH) == int:
        if len(user["bio"]) > BIO_MAX_LENGTH:
            user["bio"] = user["bio"][:BIO_MAX_LENGTH-3]+"..."
        socialMedia.append("bio:\n"+sanitize(user["bio"]))
    elif user["bio"] and BIO_MAX_LENGTH == "DYNAMIC":
        socialMedia.append("bio:\n")

    socialMedia = "\n".join(socialMedia)

    if distance:
        user["distance"] = distance

    if ROUND_DIST > 0:
        user["distance"] = round(user["distance"], ROUND_DIST)

        if user["distance"] % 1 == 0:  # don't display .0
            user["distance"] = int(user["distance"])
    else:
        user["distance"] = int(user["distance"])

    user["distance"] = str(user["distance"])

    variables = {
        **user,
        "profileURL": furmap.profileURL(user_id),
        "socialMedia": socialMedia,
        "distUnit": distUnit,
        "bot_username": BOT_USERNAME
    }

    message = getTranslation(lang, "furmap_user_card_text", **variables)

    if user["bio"] and BIO_MAX_LENGTH == "DYNAMIC":
        bio_len = MAX_MSG_LEN - len(message)
        if len(user["bio"]) > bio_len:
            user["bio"] = user["bio"][:bio_len-3]+"..."
        variables["socialMedia"] += sanitize(user["bio"])
        message = getTranslation(lang, "furmap_user_card_text", **variables)

    return message


def generateUserCard(user):
    if type(user) == str:
        user = furmap.getUserInfo(user, True)
    elif type(user) != dict:
        return False

    user_id = user["user_id"]
    pfpr = furmap.getUserImage(user_id)

    # read and crop the image
    if pfpr:
        pilpfp = Image.open(io.BytesIO(pfpr.read())).convert("RGBA")
        squarepfp = Image.new("RGBA", (200, 200), (255, 255, 255))
        scaleto = pilpfp.size[1]/200
        pilpfp = pilpfp.resize((floor(pilpfp.size[0]/scaleto), 200))
        #pilpfp = pilpfp.crop((pilpfp.size[0], 200))
        squarepfp.paste(pilpfp, (floor((200-pilpfp.size[0])/2), 0), pilpfp)
    else:
        squarepfp = False

    # generate card
    pilpfp = card(squarepfp, str(user["name"]), user)

    # convert from PIL to bytestream
    pfpcard = io.BytesIO()
    pfpcard.name = 'image.png'
    pilpfp.save(pfpcard, 'PNG')
    pfpcard.seek(0)

    return pfpcard


def getUserLocation(user_id, chat_id, reply_msg_id=None):
    user = furmap.getUserInfo(user_id, False)

    bot.send_location(chat_id,
                      user["lat"], user["lon"],
                      horizontal_accuracy=LOCATION_ACCURACY,
                      reply_to_message_id=reply_msg_id)


def sendUserMessage(update=None, context=None, chat_id=None, user_id=None, user=None, furmap_user=None, language=None, distance=None):
    query = None
    type = None
    if update and context:
        message = update.effective_message
        if update.callback_query:
            query = update.callback_query
            query_data = query.data.split("_")

            if len(query_data) >= 3:
                user_id = query_data[2]
                type = query_data[1]
                if type == "ref":
                    furmap_user = furmap.getUserInfo(user_id, True)
                    chat_id = message.chat_id
                elif type == "loc":
                    getUserLocation(user_id, message.chat_id,
                                    message.message_id)
                    query.answer()
                    return True
                else:
                    return False
            else:
                return False

            user = users.user(message.chat_id)

            if(user.exists()):
                lang = user.getLanguage(message.from_user.language_code)
            else:
                lang = message.from_user.language_code
                user.create(lang)
    elif chat_id and user_id:
        user = users.user(chat_id)
        lang = user.getLanguage(language)
        furmap_user = furmap.getUserInfo(user_id, True)
    elif chat_id and furmap_user:
        user = users.user(chat_id)
        lang = user.getLanguage(language)
    elif furmap_user and user:
        lang = user.getLanguage(language)
    else:
        return False

    acc_ok = True
    if not user.exists():
        acc_ok = False

    user_location = user.getLocation()

    if not user_location:
        acc_ok = False

    if not acc_ok:
        if not chat_id and user.exists():
            chat_id = user.user.chat_id
        elif not chat_id:
            return False
        if not type:
            bot.send_message(user.chat_id, getTranslation(
                lang or FALLBACK_LANG, "setup_location"))
        else:
            query.answer(getTranslation(lang or FALLBACK_LANG,
                         "setup_location"), show_alert=True)
        return False

    if type == "ref":
        if not user.getAndSetUserCheckLimit():
            query.answer(getTranslation(lang or FALLBACK_LANG,
                         "limit_reached"), show_alert=True)
            return False

    # proper logic begins here
    distance = geodistance(
        user_location, (furmap_user["lat"], furmap_user["lon"]))

    if (user.getDistUnit() == "km"):
        distance = distance.km
    else:
        distance = distance.miles

    userInfo = generateUserInfo(
        furmap_user, distance, user.getDistUnit(), lang, user.getPrivLinks())

    userCard = generateUserCard(furmap_user)

    keys = [
        [
            InlineKeyboardButton(getTranslation(
                lang, "refresh"), callback_data="fur_ref_"+furmap_user["user_id"])
        ],
        [
            InlineKeyboardButton(getTranslation(
                lang, "show_location"), callback_data="fur_loc_"+furmap_user["user_id"]),
            InlineKeyboardButton(getTranslation(
                lang, "visit_account"), url=furmap.profileURL(furmap_user["user_id"]))
        ]
    ]

    reply_markup = InlineKeyboardMarkup(keys)

    if not query:
        bot.send_photo(user.chat_id, userCard, caption=userInfo,
                       reply_markup=reply_markup, parse_mode=PARSE_MODE)
    else:
        try:
            query.edit_message_media(InputMediaPhoto(
                userCard, userInfo, parse_mode=PARSE_MODE), reply_markup=reply_markup)
            query.answer()
        except:
            query.answer(getTranslation(lang, "no_changes"), show_alert=True)


def checkUsers(user=None):
    return_users = True
    if user:
        user = users.user(user)
        toCheck = [user]
        return_users = False
    else:
        toCheck = users.getUsersOn()

    LOGGER.info(f"Checking {len(toCheck)} user/s")

    for user in toCheck:
        if type(user) != users.user:
            user = users.user(user)

        lang = user.getLanguage()

        accs = checkNewAccounts(user, False)

        if return_users == False:
            return_users = len(accs)

        if accs == False:
            bot.send_message(user.chat_id, getTranslation(
                lang, "setup_location"))
            return False

        if len(accs) > 0:

            shown = 0

            # send info about incoming messages
            try:
                bot.send_message(user.chat_id, getTranslation(
                    lang, "new_acc_found", len=len(accs)), parse_mode=PARSE_MODE)
                user.resetSendFails()
            except:
                user.addSendFail()

            # send info only x messages can be sent if there are more than that
            if len(accs) > MAX_ACCS_PER_REFRESH:
                bot.send_message(user.chat_id, getTranslation(
                    lang, "showing_less_users", len=len(accs)), parse_mode=PARSE_MODE)

            # get user info
            for acc in accs:
                if shown >= MAX_ACCS_PER_REFRESH:
                    break
                distance = acc["distance"]
                if not acc["full"]:
                    acc = furmap.getUserInfo(acc["user_id"], True)

                acc["distance"] = round(distance, ROUND_DIST)
                sendUserMessage(user=user, furmap_user=acc)
                user.addID(acc["user_id"])
                sleep(SLEEP_EVERY_ACC)
                shown += 1
        sleep(SLEEP_EVERY_USER)
        LOGGER.info("User/s checked")
    return return_users


def checkUsersCmd(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    if not user.getLocation():
        bot.send_message(user.chat_id, getTranslation(
            lang, "setup_location"), parse_mode=PARSE_MODE)
        return False

    if not user.getAndSetRefreshLimit():
        bot.send_message(user.chat_id, getTranslation(
            lang, "limit_reached"), parse_mode=PARSE_MODE)
        return False

    checknum = checkUsers(update.effective_message.chat_id)
    if int(checknum) < 1:
        message.reply_text(getTranslation(
            lang, "no_new_accs"), parse_mode=PARSE_MODE)


def updateDB(update, context):
    furmap.update()
    users.resetLimits()
    update.effective_message.reply_text("Updated")


def resetLimits(update, context):
    users.resetLimits()


def seen(update, context):
    message = update.effective_message
    user = users.user(message.chat_id)
    if(user.exists()):
        lang = user.getLanguage(message.from_user.language_code)
    else:
        lang = message.from_user.language_code
        user.create(lang)

    if not user.getLocation():
        bot.send_message(user.chat_id, getTranslation(lang, "setup_location"))
        return False

    if not user.getAndSetRefreshLimit():
        bot.send_message(user.chat_id, getTranslation(lang, "limit_reached"))
        return False

    seen = len(checkNewAccounts(message.chat_id))

    message.reply_text(getTranslation(
        lang, "seen", seen=seen), parse_mode=PARSE_MODE)


def update_anc_check():
    furmap.update()
    checkUsers()
    return True


dispatcher.add_handler(CommandHandler(
    getCmds("seen"), seen, Filters.chat_type.private, run_async=True))
dispatcher.add_handler(CommandHandler(
    getCmds("refresh"), checkUsersCmd, Filters.chat_type.private, run_async=True))
dispatcher.add_handler(CommandHandler(
    "updatedb", updateDB, Filters.chat_type.private & Filters.chat(ADMIN_ID), run_async=False))
dispatcher.add_handler(CommandHandler(
    "resetLimits", updateDB, Filters.chat_type.private & Filters.chat(ADMIN_ID), run_async=False))
dispatcher.add_handler(CallbackQueryHandler(
    sendUserMessage, Filters.chat_type.private, pattern="fur_", run_async=True))

import logging
import os
import sys

import telegram.ext as tg

from furmap_notifier_v2.config import BOT_TOKEN

# enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO)

LOGGER = logging.getLogger(__name__)

if sys.version_info[0] < 3 or sys.version_info[1] < 6:
    LOGGER.error("You MUST have a python version of at least 3.6! Multiple features depend on this. Bot quitting.")
    quit(1)

updater = tg.Updater(BOT_TOKEN, use_context=True)

dispatcher = updater.dispatcher

bot = dispatcher.bot